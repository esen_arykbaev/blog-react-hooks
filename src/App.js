import React from 'react';
import {BrowserRouter as Router} from "react-router-dom";
import Routers from 'pages/routers';
import {TopBar} from 'components/topBar';
import {CurrentUserProvider} from 'context/userCurrentContext';
import {CurrentUserChecker} from 'components/currentUserChecker';

function App() {
  return (
      <CurrentUserProvider>
          <CurrentUserChecker>
              <Router>
                  <TopBar/>
                  <Routers/>
              </Router>
          </CurrentUserChecker>
      </CurrentUserProvider>
  );
}

export default App;
